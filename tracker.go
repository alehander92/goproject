package yolandi

import (
	"bytes"
	"crypto/sha1"
	"encoding/binary"
	"fmt"
	"github.com/alehander42/bencodius"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
)

const (
	LISTENING_PORT  = "6689"
	FLAG_COMPACT    = "true"
	YOLANDI_ID      = "YI"
	YOLANDI_VERSION = "0200"
)

func InfoHash(torrent bencodius.BencodeDict) [20]byte {
	data := []byte(bencodius.Encode(torrent.Get("info")))
	return sha1.Sum(data)
}

func Left(torrent bencodius.BencodeDict) int {
	info := torrent.Get("info").(bencodius.BencodeDict)
	return int(info.Get("length").(bencodius.BencodeInt))
}

func GenerateTrackerQuery(torrent bencodius.BencodeDict, client Client) string {
	announce_url := string(torrent.Get("announce").(bencodius.BencodeString))
	address, err := url.Parse(announce_url)
	if err != nil {
		log.Fatal(err)
	}

	q := address.Query()
	q.Set("info_hash", string(client.InfoHash[:]))
	q.Set("port", LISTENING_PORT)
	q.Set("uploaded", "0")
	q.Set("downloaded", "0")
	q.Set("compact", FLAG_COMPACT)
	q.Set("event", "started")
	q.Set("left", fmt.Sprintf("%d", Left(torrent)))
	q.Set("peer_id", client.PeerId)

	address.RawQuery = q.Encode()
	return address.String()
}

func ParsePeersResponse(resp bencodius.BencodeValue) (peers []Peer) {
	peers = make([]Peer, 0, 50)
	switch response := resp.(type) {
	case bencodius.BencodeString:
		b := []byte(response)
		var ip []int
		var port int
		var peer Peer
		for i := 0; i < len(b); i = i + 2 {
			if i%6 == 0 {
				ip = make([]int, 4)
			}
			if i%6 < 4 {
				buf := bytes.NewReader(b[i : i+1])
				binary.Read(buf, binary.BigEndian, &ip[i])
				buf = bytes.NewReader(b[i+1 : i+2])
				binary.Read(buf, binary.BigEndian, &ip[i+1])
			} else {
				port_buf := bytes.NewReader(b[i : i+2])
				binary.Read(port_buf, binary.BigEndian, &port)

				ip_text := fmt.Sprintf("%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3])

				peer = Peer{Ip: ip_text, Port: port}
				peers = append(peers, peer)
			}
		}
	case bencodius.BencodeList:
		for _, v := range response {
			z := v.(bencodius.BencodeDict)
			peer := Peer{Ip: string(z.Get("ip").(bencodius.BencodeString)),
				Port: int(z.Get("port").(bencodius.BencodeInt))}
			if z.Exists("peer id") {
				peer.PeerId = []byte(z.Get("peer id").(bencodius.BencodeString))
			}
			peers = append(peers, peer)
		}
	}
	return
}

func GetTrackerResponse(torrent bencodius.BencodeDict, piece_base PieceDatabase) []Peer {
	client := Client{InfoHash: InfoHash(torrent), PeerId: GeneratePeerId()}
	tracker_query := GenerateTrackerQuery(torrent, client)
	resp, err := http.Get(tracker_query)
	if err != nil {
		fmt.Println("something wrong with url")
		os.Exit(1)
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	result := bencodius.Decode(string(body[:])).(bencodius.BencodeDict)
	if result.Exists("failure reason") {
		fmt.Println(result.Get("failure reason"))
		os.Exit(1)
	}
	client.Interval = int(result.Get("interval").(bencodius.BencodeInt))
	p := ParsePeersResponse(result.Get("peers"))

	for i, _ := range p {
		p[i].Client = client
		p[i].PieceBase = &piece_base
		p[i].PieceMap = NewBitList(piece_base.PieceCount)
	}
	return p

	//20-25 peers max usable
	//binary and dict model for peers
}
