package yolandi

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEncodingChoke(t *testing.T) {
	assert.New(t).Equal(
		(&Choke{}).ToBytes(),
		[]byte{0, 0, 0, 1, 0})
}

func TestEncodingUnchoke(t *testing.T) {
	assert.New(t).Equal(
		(&Unchoke{}).ToBytes(),
		[]byte{0, 0, 0, 1, 1})
}

func TestEncodingInterested(t *testing.T) {
	assert.New(t).Equal(
		(&Interested{}).ToBytes(),
		[]byte{0, 0, 0, 1, 2})
}

func TestEncodingNotInterested(t *testing.T) {
	assert.New(t).Equal(
		(&NotInterested{}).ToBytes(),
		[]byte{0, 0, 0, 1, 3})
}

func TestEncodingHave(t *testing.T) {
	assert.New(t).Equal(
		(&Have{PieceIndex: 4}).ToBytes(),
		[]byte{0, 0, 0, 5, 4, 0, 0, 0, 4})
}

func TestEncodingBitfield(t *testing.T) {
	assert.New(t).Equal(
		(&Bitfield{Field: []byte{0, 1, 0}}).ToBytes(),
		[]byte{0, 0, 0, 4, 5, 0, 1, 0})
}

func TestEncodingRequest(t *testing.T) {
	assert.New(t).Equal(
		(&Request{Index: 3, Begin: 1, Length: 3}).ToBytes(),
		[]byte{0, 0, 0, 13, 6, 0, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 3})
}

func TestEncodingPiece(t *testing.T) {
	assert.New(t).Equal(
		(&Piece{Index: 22, Begin: 0, Block: []byte{0, 1, 0}}).ToBytes(),
		[]byte{0, 0, 0, 12, 7, 0, 0, 0, 22, 0, 0, 0, 0, 0, 1, 0})
}

func TestEncodingCancel(t *testing.T) {
	assert.New(t).Equal(
		(&Cancel{Index: 3, Begin: 1, Length: 3}).ToBytes(),
		[]byte{0, 0, 0, 13, 8, 0, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 3})
}

func TestEncodingPort(t *testing.T) {
	assert.New(t).Equal(
		(&Port{ListenPort: 2}).ToBytes(),
		[]byte{0, 0, 0, 3, 9, 0, 2})
}
