package yolandi

import (
	"github.com/alehander42/bencodius"
)

const (
	BLOCK_SIZE = 16384
)

type PieceDatabase struct {
	pieces     [][][]byte
	blockMap   [][]bool
	pieceMap   []bool
	PieceCount int
}

func (base *PieceDatabase) Saved(index uint32, begin uint32) bool {
	return base.blockMap[index][begin]
}

func NewPieceDatabase(torrent bencodius.BencodeDict) PieceDatabase {
	data := torrent.Get("info").(bencodius.BencodeDict)
	length := int(data.Get("length").(bencodius.BencodeInt))
	piece_length := int(data.Get("piece length").(bencodius.BencodeInt))

	base := PieceDatabase{}
	base.pieces = make([][][]byte, (length/piece_length)+1)
	base.blockMap = make([][]bool, (length/piece_length)+1)
	base.pieceMap = make([]bool, (length/piece_length)+1)
	base.PieceCount = length/piece_length + 1

	for j, _ := range base.pieces {
		base.pieces[j] = make([][]byte, (piece_length/BLOCK_SIZE)+1)
		base.blockMap[j] = make([]bool, (piece_length/BLOCK_SIZE)+1)
	}
	return base
}

func (base *PieceDatabase) save_block(index uint32, begin uint32, block []byte) {
	base.pieces[index][begin] = block[:BLOCK_SIZE]
	base.blockMap[index][begin] = true
	flag := true
	for _, block := range base.blockMap[index] {
		if !block {
			flag = false
			break
		}
	}
	if flag {
		base.pieceMap[index] = true
	}
}
