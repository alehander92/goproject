package yolandi

import (
	"bytes"
	"errors"
	"fmt"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

type Peer struct {
	PeerId     []byte
	Ip         string
	Port       int
	Conn       net.Conn
	Field      chan bool
	Unchoked   chan bool
	Interested chan bool
	PieceMap   BitList
	PieceBase  *PieceDatabase
	Client     Client
}

func (peer *Peer) Address() string {
	return fmt.Sprintf("%s:%d", peer.Ip, peer.Port)
}

func (peer *Peer) MatchingHandshake(handshake []byte) bool {
	if peer.PeerId == nil {
		return true
	} else {
		return bytes.Equal(peer.PeerId, HandshakeInfoHash(handshake))
	}
}

func (peer *Peer) Read(buffer []byte) bool {
	_, err := peer.Conn.Read(buffer)
	if err != nil {
		peer.Conn.Close()
		return false
	}
	return true
}

func (peer *Peer) ReadMessage(buffer []byte, field chan bool, unchoked chan bool, interested chan bool) uint32 {
	parsed_message, length := ParseMessage(buffer)
	//fmt.Println(parsed_message)
	switch message := parsed_message.(type) {
	case *Choke:
		unchoked <- false
		// peer.Unchoked <- false
	case *Unchoke:
		unchoked <- true
		// peer.Unchoked <- true
	case *Interested:
		interested <- true
		// peer.Interested <- true
	case *NotInterested:
		interested <- false
		// peer.Interested <- false
	case *Have:
		peer.PieceMap.positive(message.PieceIndex)
		peer.Field <- true
	case *Bitfield:
		peer.PieceMap.merge(message.Field)
		fmt.Println("updated piece map to", peer.PieceMap,
			"with", message.Field)
		field <- true
		// peer.Field <- true
	case *Request:
		// give them code
	case *Piece:
		fmt.Println("received piece with", message.Index, message.Begin, len(message.Block))
		peer.PieceBase.save_block(message.Index, message.Begin, message.Block)
	case *Cancel:
		// stop giving them code
	case *Port:
		// do nothing
	}
	return length
}

func (peer *Peer) Transmit(z chan bool) {
	// peer.Unchoked <- false
	// peer.Interested <- false

	fmt.Println("transmit", peer)
	h := peer.Handshake()
	if !h {
		return
	}

	defer peer.Conn.Close()

	field := make(chan bool)
	unchoked := make(chan bool)
	interested := make(chan bool)

	go peer.SendInterested(field)
	go peer.SendRequests(unchoked)
	go peer.SendDrop(interested)
	go peer.Receive(field, unchoked, interested)
	z <- true
}

func (peer *Peer) Receive(field chan bool, unchoked chan bool, interested chan bool) {
	buf := make([]byte, 17000)
	// old_buf := buf[:0]

	for peer.Read(buf) {
		// buf = append(old_buf, buf...)
		fmt.Println(buf[:9])
		fmt.Println("receive from", peer.Ip)

		rec := peer.ReadMessage(buf, field, unchoked, interested)
		fmt.Println("rec ", rec)
		if rec < 17000 {
			fmt.Println(buf[rec : rec+20])
		}

		// old_buf = buf[rec+4:]

		// for i := rec + 4; i < rec + 4; i++ {
		// buf[i] = 0
		// }
	}
}

func (peer *Peer) SendInterested(field chan bool) {
	for {
		fmt.Println("send interested to", peer.Ip)
		field := <-field // peer.Field
		if field {
			fmt.Println("send interested sexy")
			peer.Conn.Write((&Interested{}).ToBytes())
		} else {
			fmt.Println("wtf sex")
		}
	}
}

func (peer *Peer) SendDrop(interested chan bool) {
	for {
		status := <-interested
		if status {
			fmt.Println("choke")
			peer.Conn.Write((&Choke{}).ToBytes())
		}
	}
}

func (peer *Peer) SendRequests(unchoked chan bool) {
	for {
		status := <-unchoked
		if status {
			request, err := peer.FindRequest()
			if err != nil {
				peer.Conn.Close()
				return // nothing to get from that peer
			}
			fmt.Println("send request", request)
			peer.Conn.Write(request.ToBytes())
		}
	}
}

func (peer *Peer) FindRequest() (Request, error) {
	// should not be saved in piece base and should appear in peers map
	for i, bits_byte := range peer.PieceMap.Bits {
		var u uint32
		for u = 0; u < 8; u++ {
			bit := uint8((bits_byte >> u) & 1)
			if bit == 1 && !peer.PieceBase.Saved(uint32(i), u) {
				return Request{Index: uint32(i), Begin: u, Length: BLOCK_SIZE}, nil
			}
		}
	}
	return Request{}, errors.New("no feasible request for peer")
}

func HandshakeInfoHash(handshake []byte) []byte {
	return handshake[48:]
}

func handleErr(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func GeneratePeerId() string {
	// process ID and perhaps a timestamp recorded at startup to be
	// unique for the machine
	rand.Seed(time.Now().UnixNano())
	random_number := rand.Intn(1000000000000)
	return fmt.Sprintf("-%s%s-%12d", YOLANDI_ID, YOLANDI_VERSION, random_number)
}

func (client *Client) GenerateHandshake() []byte {
	handshake := make([]byte, 1, 68)
	handshake[0] = 19

	handshake = append(handshake, []byte("BitTorrent protocol")...)
	fmt.Println(handshake)
	handshake = append(handshake, []byte{0, 0, 0, 0, 0, 0, 0, 0}...)
	handshake = append(handshake, client.InfoHash[:]...)
	return append(handshake, []byte(client.PeerId)...)
}

func ConnectPeers(peers []Peer) {
	fmt.Println("connect")
	if len(peers) > 20 {
		peers = peers[:20]
	}

	q := make([]chan bool, len(peers))

	// for i, peer := range peers {
	peer := peers[10]
	fmt.Println(peer)
	go peer.Transmit(q[2])
	// }
	r := <-q[0]
	fmt.Println(r)
}

func (peer *Peer) Connect() (net.Conn, error) {
	ip := make([]byte, 4, 16)
	pb := strings.Split(peer.Ip, ".")

	for i := 0; i < 4; i++ {
		ip_byte, _ := strconv.Atoi(pb[i])
		ip[i] = byte(ip_byte)
	}

	addr := net.TCPAddr{Port: peer.Port, IP: net.IP(ip)}
	return net.DialTCP("tcp", nil, &addr)
}

func (peer *Peer) Handshake() bool {
	handshake_message := peer.Client.GenerateHandshake()
	fmt.Println("hand")
	fmt.Println(handshake_message)
	fmt.Println(peer.Address())

	var err error

	peer.Conn, err = peer.Connect()
	handleErr(err)

	// conn, err := net.Dial("tcp", peer.Address())
	_, err = peer.Conn.Write(handshake_message)
	if err != nil {
		peer.Conn.Close()
		return false
	}

	reply := make([]byte, 68)
	_, err = peer.Conn.Read(reply)
	if err != nil {
		peer.Conn.Close()
		return false
	}

	fmt.Println("reply")
	fmt.Println(reply)

	h := peer.MatchingHandshake(reply)
	fmt.Println(h)
	if !h {
		peer.Conn.Close()
		return false
	}
	return true
}
