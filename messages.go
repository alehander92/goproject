package yolandi

import (
	"encoding/binary"
	"fmt"
)

type Message interface {
	ToBytes() []byte
}

type KeepAlive struct {
}

func (message *KeepAlive) ToBytes() []byte {
	return make([]byte, 1)
}

type Choke struct {
}

func (message *Choke) ToBytes() []byte {
	return []byte{0, 0, 0, 1, 0}
}

type Unchoke struct {
}

func (message *Unchoke) ToBytes() []byte {
	return []byte{0, 0, 0, 1, 1}
}

type Interested struct {
}

func (message *Interested) ToBytes() []byte {
	return []byte{0, 0, 0, 1, 2}
}

type NotInterested struct {
}

func (message *NotInterested) ToBytes() []byte {
	return []byte{0, 0, 0, 1, 3}
}

type Have struct {
	PieceIndex uint32
}

func (message *Have) ToBytes() []byte {
	b := make([]byte, 0, 9)
	b = append(b, []byte{0, 0, 0, 5, 4, 0, 0, 0, 0}...)
	binary.BigEndian.PutUint32(b[5:9], message.PieceIndex)
	return b
}

type Bitfield struct {
	Field []byte
}

func (message *Bitfield) ToBytes() []byte {
	b := make([]byte, 5, 5)
	binary.BigEndian.PutUint32(b[:4], uint32(1+len(message.Field)))
	b[4] = 5
	return append(b, message.Field...)
}

type Request struct {
	Index  uint32
	Begin  uint32
	Length uint32
}

func (message *Request) ToBytes() []byte {
	b := make([]byte, 0, 17)
	b = append(b, []byte{0, 0, 0, 13, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}...)
	binary.BigEndian.PutUint32(b[5:9], message.Index)
	binary.BigEndian.PutUint32(b[9:13], message.Begin)
	binary.BigEndian.PutUint32(b[17-4:17], message.Length)
	return b
}

type Piece struct {
	Index uint32
	Begin uint32
	Block []byte
}

func (message *Piece) ToBytes() []byte {
	b := make([]byte, 13, 13+len(message.Block))
	binary.BigEndian.PutUint32(b[:4], uint32(9+len(message.Block)))
	b[4] = 7 // id
	binary.BigEndian.PutUint32(b[5:9], message.Index)
	binary.BigEndian.PutUint32(b[9:13], message.Begin)
	return append(b, message.Block...)
}

type Cancel struct {
	Index  uint32
	Begin  uint32
	Length uint32
}

func (message *Cancel) ToBytes() []byte {
	b := make([]byte, 0, 17)
	b = append(b, []byte{0, 0, 0, 13, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}...)
	binary.BigEndian.PutUint32(b[5:9], message.Index)
	binary.BigEndian.PutUint32(b[9:13], message.Begin)
	binary.BigEndian.PutUint32(b[13:17], message.Length)
	return b
}

type Port struct {
	ListenPort uint16
}

func (message *Port) ToBytes() []byte {
	b := make([]byte, 0, 7)
	b = append(b, []byte{0, 0, 0, 3, 9, 0, 0}...)
	binary.BigEndian.PutUint16(b[5:7], message.ListenPort)
	return b
}

func toUint32(input []byte) uint32 {
	return binary.BigEndian.Uint32(input)
}

func ParseMessage(input []byte) (message Message, length uint32) {
	length = binary.BigEndian.Uint32(input[:4])

	if length == 0 {
		message = &KeepAlive{}
		fmt.Println(0, 0)
		return
	}
	id := input[4]

	fmt.Println(length, id)
	switch id {
	case 0:
		message = &Choke{}
	case 1:
		message = &Unchoke{}
	case 2:
		message = &Interested{}
	case 3:
		message = &NotInterested{}
	case 4:
		message = &Have{PieceIndex: toUint32(input[5:9])}
	case 5:
		message = &Bitfield{Field: input[5 : 4+length]}
	case 6:
		message = &Request{Index: toUint32(input[5:9]),
			Begin:  toUint32(input[9:13]),
			Length: toUint32(input[13:17])}
	case 7:
		fmt.Println(len(input) - 4)
		message = &Piece{Index: toUint32(input[5:9]),
			Begin: toUint32(input[9:13]),
			Block: input[13 : 4+length]}
	case 8:
		message = &Cancel{Index: toUint32(input[5:9]),
			Begin:  toUint32(input[9:13]),
			Length: toUint32(input[13:17])}
	case 9:
		message = &Port{ListenPort: binary.BigEndian.Uint16(input[5:7])}
	default:
		message = &KeepAlive{}
		// panic("unrecognizable id of message")
	}
	return
}
