package yolandi

type Client struct {
	InfoHash [20]byte
	PeerId   string
	Interval int
}
