package yolandi

import (
	"fmt"
	"github.com/alehander42/bencodius"
	"io/ioutil"
	"strings"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func DecodeTorrentFile(torrent_filename string) bencodius.BencodeDict {
	bytes, err := ioutil.ReadFile(torrent_filename)
	content := string(bytes[:])
	check(err)

	return bencodius.Decode(content).(bencodius.BencodeDict)
}

func DisplayHuman(data bencodius.BencodeValue) string {
	var out string
	switch v := data.(type) {
	case bencodius.BencodeInt:
		out = fmt.Sprintf("%d", v)
	case bencodius.BencodeString:
		out = fmt.Sprintf("\"%s\"", v)
	case bencodius.BencodeList:
		result := []string{}
		for _, a := range v {
			result = append(result, DisplayHuman(a))
		}
		out = fmt.Sprintf("[%s]", strings.Join(result, ",  "))
	case bencodius.BencodeDict:
		result := []string{}
		for _, y := range v.Keys {
			if y != "pieces" {
				z := v.Dict[y]
				result = append(result, fmt.Sprintf("%s : %s",
					DisplayHuman(y), DisplayHuman(z)))
			}
		}
		out = fmt.Sprintf("{%s}", strings.Join(result, ",  "))
	}
	return out
}
