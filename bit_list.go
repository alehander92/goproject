package yolandi

const (
	BYTE_SIZE = 8
)

type BitList struct {
	Bits []byte
}

func NewBitList(size int) BitList {
	list := BitList{Bits: make([]byte, size)}
	return list
}

func (list *BitList) positive(index uint32) {
	a := byte(1 << uint(index%BYTE_SIZE))
	list.Bits[index/BYTE_SIZE] = list.Bits[index/BYTE_SIZE] | a
}

func (list *BitList) negative(index uint32) {
	a := byte(^(1 << uint(index%BYTE_SIZE)))
	list.Bits[index/BYTE_SIZE] = list.Bits[index/BYTE_SIZE] & a
}

func (list *BitList) merge(Bits []byte) {
	for i, b := range list.Bits {
		if i < len(Bits) {
			list.Bits[i] = b | Bits[i]
		}
	}
}
