package main

import (
	"flag"
	"fmt"
	"os"
	"yolandi"
)

func main() {

	var (
		parse            bool
		connect          bool
		torrent_filename string
	)

	flag.BoolVar(&parse, "parse", true, "parse torrent file")
	flag.BoolVar(&parse, "p", true, "parse shorthand")
	flag.BoolVar(&connect, "connect-tracker", false, "connect to tracker")
	flag.BoolVar(&connect, "c", false, "connect shorthand")

	fmt.Println(parse)
	fmt.Println(connect)
	//TODO fix later because flag is weird
	if len(os.Args) == 1 {
		flag.Usage()
		os.Exit(1)
	} else {
		torrent_filename = os.Args[1]
	}

	fmt.Println("lala")
	fmt.Println(torrent_filename)
	// parsing
	data := yolandi.DecodeTorrentFile(torrent_filename)
	fmt.Println(yolandi.DisplayHuman(data))
	a := yolandi.NewPieceDatabase(data)
	// connecting
	e := yolandi.GetTrackerResponse(data, a)
	yolandi.ConnectPeers(e)

	// q := make(chan bool)
	// x := make(chan bool)
	// go f(q, x)
	// go g(q, x)
	// e := <-x
	// fmt.Println(e)
}

// func f(q chan bool, x chan bool) {
// 	q <- false
// 	q <- true
// }

// func g(q chan bool, x chan bool) {
// 	for {
// 		s := <-q
// 		if s {
// 			fmt.Println("yes")
// 			x <- true
// 		} else {
// 			fmt.Println("wtf")
// 		}
// 	}
// }
